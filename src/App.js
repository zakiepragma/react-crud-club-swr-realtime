import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import AddClub from "./components/AddClub";
import ClubList from "./components/ClubList";
import EditClub from "./components/EditClub";

const App = () => {
  return (
    <div className="mx-auto max-w-screen-lg">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<ClubList />} />
          <Route path="/add" element={<AddClub />} />
          <Route path="/edit/:id" element={<EditClub />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
