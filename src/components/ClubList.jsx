import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import useSWR, { mutate, useSWRConfig } from "swr";

const ClubList = () => {
  const fetcher = async () => {
    const response = await axios.get("http://localhost:5000/clubs");
    return response.data;
  };

  const { data } = useSWR("clubs", fetcher);

  if (!data) return <h2>Loading...</h2>;

  const deleteProduct = async (productId) => {
    await axios.delete(`http://localhost:5000/clubs/${productId}`);
    mutate("clubs");
  };

  return (
    <div className="flex flex-col mt-5">
      <div className="w-full">
        <Link
          to="/add"
          className="bg-green-500 hover:bg-green-700 border border-slate-200 text-white font-bold py-2 px-4 rounded-lg"
        >
          Add New Club
        </Link>
        <div className="relative shadow rounded-lg mt-3">
          <table className="w-full text-sm text-left text-gray-500">
            <thead className="text-xs text-gray-900 uppercase bg-gray-100">
              <tr>
                <th className="py-3 px-1 text-center">#</th>
                <th className="py-3 px-6">Club Name</th>
                <th className="py-3 px-6">Point</th>
                <th className="py-3 px-1 text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
              {data.map((club, index) => (
                <tr className="bg-white border-b" key={club.id}>
                  <td className="py-3 px-1 text-center">{index + 1}</td>
                  <td className="py-3 px-6 font-medium text-gray-900">
                    {club.name}
                  </td>
                  <td className="py-3 px-6">{club.point}</td>
                  <td className="py-3 px-1 text-center">
                    <Link
                      to={`/edit/${club.id}`}
                      className="font-medium bg-blue-400 hover:bg-blue-500 px-3 py-1 rounded text-white mr-1"
                    >
                      Edit
                    </Link>
                    <button
                      onClick={() => deleteProduct(club.id)}
                      className="font-medium bg-red-400 hover:bg-red-500 px-3 py-1 rounded text-white"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default ClubList;
