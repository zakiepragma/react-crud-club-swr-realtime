import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

const EditClub = () => {
  const [name, setName] = useState("");
  const [point, setPoint] = useState(0);
  const navigate = useNavigate();
  const { id } = useParams();

  const updateClub = async (e) => {
    e.preventDefault();

    await axios.patch(`http://localhost:5000/clubs/${id}`, {
      name: name,
      point: parseInt(point),
    });
    navigate("/");
  };

  useEffect(() => {
    const getClubById = async () => {
      const response = await axios.get(`http://localhost:5000/clubs/${id}`);
      setName(response.data.name);
      setPoint(response.data.point);
    };
    getClubById();
  }, [id]);

  return (
    <div className="max-w-lg mx-auto my-10 bg-white p-8 rounded-xl shadow shadow-slaye-300">
      <form onSubmit={updateClub} className="my-10">
        <div className="flex flex-col">
          <div className="mb-5">
            <label className="font-bold text-slate-700">Club Name</label>
            <input
              type="text"
              className="w-full py-3 mt-1 border border-slate-200 rounded-lg px-3 focus:outline-none focus:border-slate-500 hover:shadow"
              placeholder="Club Name"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="mb-5">
            <label className="font-bold text-slate-700">Point</label>
            <input
              type="text"
              className="w-full py-3 mt-1 border border-slate-200 rounded-lg px-3 focus:outline-none focus:border-slate-500 hover:shadow"
              placeholder="Point"
              value={point}
              onChange={(e) => setPoint(e.target.value)}
            />
          </div>
          <button
            type="submit"
            className="w-full py-3 font-bold text-white bg-orange-600 hover:bg-orange-800 rounded-lg border-orange-500 hover:shadow"
          >
            Update
          </button>
        </div>
      </form>
    </div>
  );
};

export default EditClub;
